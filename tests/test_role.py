# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ansible_analyze_tools.model.role import Role

ROLES_DIR = 'resources/ansible/roles'


class TestRole(unittest.TestCase):
    """Tests de la classe Role."""

    def test_constructor(self):
        """Teste l'appel au constructeur"""
        r = Role(ROLES_DIR, 'role_empty1')
        self.assertIsNotNone(r)

    def test_constructor_error(self):
        """Teste que le constructeur vérifie l'existance du rôle"""
        self.assertRaises(ValueError, Role, ROLES_DIR, 'inexistant_role')

    def test_role_name(self):
        """Teste la récupération du nom du rôle"""
        r = Role(ROLES_DIR, 'role_meta')
        self.assertEqual('role_meta', r.name)

    def test_find_role_with_meta(self):
        """Teste la récupération des rôles via le fichier meta"""
        r = Role(ROLES_DIR, 'role_meta')
        self.assertEqual(2, len(r.roles))
        self.assertEqual('role_empty1', r.roles[0].name)
        self.assertEqual('role_empty2', r.roles[1].name)

    def test_find_role_with_include(self):
        """Teste la récupération des rôles via les tâches include_role et import_role"""
        r = Role(ROLES_DIR, 'role_include')
        self.assertEqual(2, len(r.roles))
        self.assertEqual('role_empty2', r.roles[0].name)
        self.assertEqual('role_empty3', r.roles[1].name)

    def test_to_json(self):
        """Teste la conversion de l'objet au format JSON"""
        r = Role(ROLES_DIR, 'role_meta')
        rj = r.to_json()
        # print(json.dumps(rj, indent=4))
        self.assertEqual(2, len(rj['roles']))
        self.assertEqual('role_meta', rj['name'])
        self.assertEqual('role_empty1', rj['roles'][0]['name'])
        self.assertEqual('role_empty2', rj['roles'][1]['name'])

    def test_getting_dependencies(self):
        """Teste la récupération des dépendances"""
        r = Role(ROLES_DIR, 'role_meta')
        self.assertEqual(2, len(r.dependencies()))
        self.assertEqual(2, len(r.roles))

    def test_eager_loading(self):
        """Teste la recherche en profondeur"""
        r = Role(ROLES_DIR, 'role_super')
        self.assertEqual(3, len(r.roles))
        self.assertEqual(4, sum(len(child.roles) for child in r.roles))

    def test_lazy_loading(self):
        """Teste la recherche limitée au seul rôle initial"""
        r = Role(ROLES_DIR, 'role_super', eager=False)
        self.assertEqual(3, len(r.roles))
        self.assertEqual(0, sum(len(child.roles) for child in r.roles))

    def test_has_tasks(self):
        """Teste la bonne identification de tâches sur un rôle"""
        r = Role(ROLES_DIR, 'role_meta')
        self.assertFalse(r.has_tasks())
        r = Role(ROLES_DIR, 'role_empty1')
        self.assertTrue(r.has_tasks())


if __name__ == '__main__':
    unittest.main()
