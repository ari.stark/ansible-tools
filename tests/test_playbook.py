# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from ansible_analyze_tools.model.playbook import Playbook

ANSIBLE_DIR = 'resources/ansible'


class TestPlaybook(unittest.TestCase):
    """Tests de la classe Playbook."""

    def test_contructor(self):
        """Teste l'appel au constructeur"""
        p = Playbook('%s/play_roles.yml' % ANSIBLE_DIR)
        self.assertIsNotNone(p)

    def test_contructor_error(self):
        """Teste que le constructeur vérifie l'existence du fichier"""
        self.assertRaises(ValueError, Playbook, '%s/playyy.yml' % ANSIBLE_DIR)

    def test_find_playbooks_in_playbook(self):
        """Teste que les imports de playbooks sont trouvés"""
        p = Playbook('%s/play_import_play.yml' % ANSIBLE_DIR)
        self.assertEqual(2, len(p.playbooks))
        self.assertEqual(0, len(p.roles))
        self.assertEqual('play_roles.yml', p.playbooks[0].name)
        self.assertEqual('play_empty1.yml', p.playbooks[1].name)

    def test_find_roles_in_playbooks_in_playbook(self):
        """Teste que les imports de rôles dans les playbooks inclus sont trouvés"""
        p = Playbook('%s/play_import_play.yml' % ANSIBLE_DIR)
        self.assertCountEqual(['role_empty1', 'role_empty2'], [r.name for r in p.playbooks[0].roles])

    def test_find_roles_with_roles(self):
        """Teste que les rôles sont trouvés via la section roles"""
        p = Playbook('%s/play_roles.yml' % ANSIBLE_DIR)
        self.assertEqual(0, len(p.playbooks))
        self.assertEqual(2, len(p.roles))
        self.assertCountEqual(['role_empty1', 'role_empty2'], [r.name for r in p.roles])

    def test_find_roles_with_import_roles(self):
        """Teste que les rôles sont trouvés via les tâches import_role"""
        p = Playbook('%s/play_import_roles.yml' % ANSIBLE_DIR)
        self.assertEqual(0, len(p.playbooks))
        self.assertEqual(2, len(p.roles))
        self.assertCountEqual(['role_empty1', 'role_empty3'], [r.name for r in p.roles])

    def test_find_roles_with_include_roles(self):
        """Teste que les rôles sont trouvés via les tâches include_role"""
        p = Playbook('%s/play_include_roles.yml' % ANSIBLE_DIR)
        self.assertEqual(0, len(p.playbooks))
        self.assertEqual(2, len(p.roles))
        self.assertCountEqual(['role_empty2', 'role_empty3'], [r.name for r in p.roles])

    def test_to_json(self):
        """Teste la conversion de l'objet au format JSON"""
        p = Playbook('%s/play_import_play.yml' % ANSIBLE_DIR)
        pjson = p.to_json()
        # print(json.dumps(pjson, indent=4))

        self.assertEqual(2, len(pjson['playbooks']))
        self.assertNotIn('roles', pjson)
        self.assertEqual('play_roles.yml', pjson['playbooks'][0]['name'])
        self.assertEqual(2, len(pjson['playbooks'][0]['roles']))

    def test_getting_dependencies(self):
        """Teste le regroupement des dépendances"""
        p = Playbook('%s/play_import_both.yml' % ANSIBLE_DIR)
        self.assertEqual(2, len(p.dependencies()))
        self.assertEqual(1, len(p.playbooks))
        self.assertEqual(1, len(p.roles))


if __name__ == '__main__':
    unittest.main()
