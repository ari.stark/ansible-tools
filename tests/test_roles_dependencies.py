# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import roles_dependencies

ACTUAL_FILE_NAME = '/tmp/actual_graph_roles.gv'
EXPECTED_FILE_NAME = 'resources/graphviz/expected_graph_roles.gv'


class TestRolesDependencies(unittest.TestCase):
    """Tests de non-régression pour le script roles_dependencies.py"""

    def test_no_regression(self):
        """Teste la non-régression"""
        self.maxDiff = None
        roles_dependencies.create_graph('resources/ansible/roles',
                                        ACTUAL_FILE_NAME, want_image=False)
        # self.assertTrue(filecmp.cmp(EXPECTED_FILE_NAME, ACTUAL_FILE_NAME))

        # Pour plus de détails sur la différence.
        with open(ACTUAL_FILE_NAME, 'r') as f:
            actual = f.readlines()
        with open(EXPECTED_FILE_NAME, 'r') as f:
            expected = f.readlines()
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
