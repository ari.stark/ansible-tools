#!/usr/bin/python3
# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import argparse
import os

from graphviz import Digraph

from ansible_analyze_tools.model.playbook import Playbook

graph = Digraph()

# Stockage pour éviter les doublons
nodes_id = []
links = []


def add_node(element) -> None:
    """
    Ajoute un nœud au graphe.

    Cette fonction vérifie que le nœud n'a pas déjà été ajouté, pour éviter les doublons dans le graphe.

    :param element: Élement à insérer : un playbook ou un rôle.
    """
    if element.name not in nodes_id:
        has_tasks = isinstance(element, Playbook) or element.has_tasks()
        graph.node(element.name,
                   style='solid' if has_tasks else 'dotted')  # S'il n'y a pas de tâche, le nœud est en pointillé.
        nodes_id.append(element.name)


def add_dependencies(parent, child) -> None:
    """
    Ajoute une relation de dépendance entre un nœud parent et un nœud enfant.

    Cette fonction est récursive, elle ajoute le nœud enfant et crée le lien avec le parent.
    Puis, pour chaque petit-enfant, elle s'appelle de nouveau.

    :param parent: Nœud parent (par exemple le playbook ou un rôle).
    :param child: Nœud enfant (probablement un rôle)
    """
    # Ajout du nœud et du lien.
    add_node(child)
    if [parent.name, child.name] not in links:
        graph.edge(parent.name, child.name)
        links.append([parent.name, child.name])

    # Ajout des dépendances pour chaque dépendance dont dépend la dépendance.
    for grand_child in child.dependencies():
        add_dependencies(child, grand_child)


def create_graph(playbook_name, output_file_name, want_image) -> None:
    """
    Génère un graphe de dépendances de rôles d'un playbook.

    Cette fonction crée le graphe et initialise la récursion, puis affiche le graphe.

    :param playbook_name: Nom du fichier du playbook.
    :param output_file_name: Nom du fichier de sortie.
    :param want_image: Booléen indiquant s'il faut une image ou un fichier source en sortie.
    """
    master_playbook = Playbook(playbook_name)

    # Crée un graphe orienté.
    global graph
    graph = Digraph('dependencies', engine='dot')
    graph.attr('edge', dir='back', arrowtail='odiamond')

    # Ajoute la racine du graphe.
    add_node(master_playbook)

    for dependency in master_playbook.dependencies():
        add_dependencies(master_playbook, dependency)

    # Écrit le graphe.
    if want_image:
        graph.render(output_file_name, format='png', view=True, cleanup=True)
    else:
        graph.save(output_file_name)


# main
if __name__ == '__main__':
    DEFAULT_OUTPUT_FILE = '/tmp/graph-playbook-dependencies.gv'

    parser = argparse.ArgumentParser(description='Build a directed graph of dependencies of a playbook.')
    parser.add_argument('playbook',
                        help='Path of the playbook.',
                        )
    parser.add_argument('-r', '--roles-path',
                        help='Path to roles. (Default: roles directory in playbook path)',
                        )
    parser.add_argument('-o', '--output-file',
                        help='Output file name. (Default: {})'.format(DEFAULT_OUTPUT_FILE),
                        default=DEFAULT_OUTPUT_FILE,
                        )
    output_group = parser.add_mutually_exclusive_group()
    output_group.add_argument('-i', '--image-file',
                              help='Output file as a PNG file. (Default: True)',
                              default=True,
                              action='store_true',
                              dest='image_file',
                              )
    output_group.add_argument('-t', '--text-file',
                              help='Output file as a text file.',
                              action='store_false',
                              dest='image_file',
                              )
    args = parser.parse_args()

    # Définit des valeurs par défaut dynamiquement.
    if args.roles_path is None:
        args.roles_path = os.path.join(os.path.dirname(args.playbook), 'roles')
    args.playbooks_path = os.path.dirname(args.playbook)

    if not os.path.isfile(args.playbook):
        raise ValueError("Playbook '{}' doesn't exist.".format(args.playbook))

    if not os.path.isdir(args.roles_path):
        raise ValueError("Roles path '{}' doesn't exist or is not a directory.".format(args.roles_path))

    create_graph(args.playbook, args.output_file, args.image_file)
