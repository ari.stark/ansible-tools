# Copyright:
#  (c) 2017, Ansible Project
#  (c) 2019, Ari Stark
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible.plugins.action import ActionBase
from ansible.errors import AnsibleError
from ansible.module_utils.six.moves import shlex_quote


class ActionModule(ActionBase):

    def run(self, tmp=None, task_vars=None):
        del tmp  # tmp no longer has any effect

        # login_shell module is implemented via command
        self._task.action = 'command'

        args = self._task.args
        args['_uses_shell'] = True

        # assigning cmd to _raw_params only once :
        # doing it only once is useless most of the time, but needed in case we're in a retries loop
        if '_raw_params' not in args:
            if 'cmd' not in args:  # cmd parameter is mandatory
                raise AnsibleError('cmd is required')

            args['_raw_params'] = ' '.join(
                [args['executable'] if 'executable' in args else '/bin/sh', '-lc', shlex_quote(args['cmd'])])
            del args['cmd']

        command_action = self._shared_loader_obj.action_loader.get('command',
                                                                   task=self._task,
                                                                   connection=self._connection,
                                                                   play_context=self._play_context,
                                                                   loader=self._loader,
                                                                   templar=self._templar,
                                                                   shared_loader_obj=self._shared_loader_obj)
        result = command_action.run(task_vars=task_vars)

        return result
