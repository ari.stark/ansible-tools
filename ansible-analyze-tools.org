#+SETUPFILE: ~/.emacs.d/var/theme-bigblow.setup
#+SEQ_TODO: TODO(t) STARTED(s) | DONE(d)
#+SEQ_TODO: | CANCELED(c)
#+TAGS: dev(d)
#+OPTIONS: p:t
#+CATEGORY: a-a-tool
#+TITLE: Projet d'analyse du code Ansible

* Activités [0/3]
  :PROPERTIES:
  :COOKIE_DATA: todo recursive
  :END:

** TODO Refactoring du parcours récursif
   Le parcours récursif peut parcourir des nœuds en doublon dans la mesure où le graphe n'est pas un arbre.

** TODO Corriger le problème des tests avec GitLab CI
   Le test de non-régression ne fonctionne pas, car l'ordre de parcours n'est pas le même.

** TODO Ajouter la fonctionnalité de trouver les rôles non utilisés
   Certains rôles peuvent ne jamais être utilisés, il faut parcourir l'arborescence pour les identifier.
   Il faut faire attention aux noms des rôles qui seraient générés dynamiquement (si jamais c'est possible).
