# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import os

import yaml

from ansible_analyze_tools.model.role import Role

ROLE_DIR_SUFFIX = 'roles'


class Playbook:
    """
    Modélisation simplifiée d'un playbook Ansible.

    Ce modèle ne prend en compte que les dépendances vis-à-vis des playbooks inclus et des rôles.
    """

    name = None
    root_dir = None
    role_dir = None
    file_name = None

    playbooks = None
    roles = None

    def __init__(self, file_name) -> None:
        """
        Construit un modèle de playbook.

        Le constructeur lit le fichier correspondant au playbook
        et va ensuite charger toutes les dépendances de manière avide.

        :param file_name: Nom du playbook à modéliser.
        """
        super().__init__()

        self.root_dir = os.path.dirname(os.path.expanduser(file_name))
        self.name = os.path.basename(os.path.expanduser(file_name))
        self.role_dir = os.path.join(self.root_dir, ROLE_DIR_SUFFIX)
        self.file_name = os.path.expanduser(file_name)

        if not os.path.isfile(self.file_name):
            raise ValueError(self.file_name + ' is not a file')

        self._load()

    def to_json(self) -> dict:
        """
        Convertie l'objet au format JSON.

        :return: Une structure JSON.
        """
        json = {
            'name': self.name
        }
        if self.playbooks:
            playbooks = []
            for playbook in self.playbooks:
                playbooks.append(playbook.to_json())
            json['playbooks'] = playbooks
        if self.roles:
            roles = []
            for role in self.roles:
                roles.append(role)
            json['roles'] = roles
        return json

    def _load(self) -> None:
        """
        Charge le contenu du fichier.
        """
        with open(self.file_name, 'r') as file_desc:
            yaml_content = yaml.load(file_desc.read(), Loader=yaml.FullLoader)

        if yaml_content is None:  # Fichier vide ? Rien à faire.
            return

        self._find_playbooks(yaml_content)
        self._find_roles(yaml_content)

    def _find_playbooks(self, yaml_content) -> None:
        """
        Trouve les playbooks inclus.

        :param yaml_content: Contenu du playbook.
        """
        self.playbooks = []
        for play in yaml_content:
            # Les imports de playbook se font directement à la racine via import_playbook ...
            if 'import_playbook' in play:
                child = Playbook(os.path.join(self.root_dir, play['import_playbook']))
                self.playbooks.append(child)
            # ... et include.
            if 'include' in play:
                child = Playbook(os.path.join(self.root_dir, play['include']))
                self.playbooks.append(child)

    def _find_roles(self, yaml_content) -> None:
        """
        Trouve les rôles inclus.

        :param yaml_content: Contenu du playbook.
        """
        self.roles = []
        for play in yaml_content:
            # Les imports de rôles se font dans un play, via la section "roles"...
            for role in play['roles'] if 'roles' in play else []:
                role_name = role['role'] if (isinstance(role, dict)) else role
                self.roles.append(Role(self.role_dir, role_name))
            # ... ou avec les tâches import_role ...
            for task in play['tasks'] if 'tasks' in play else []:
                if 'import_role' in task:
                    self.roles.append(Role(self.role_dir, task['import_role']['name']))
            # ... et include_role.
            for task in play['tasks'] if 'tasks' in play else []:
                if 'include_role' in task:
                    self.roles.append(Role(self.role_dir, task['include_role']['name']))

    def dependencies(self) -> list:
        """
        Regroupe les dépendances.

        :return: Liste assemblée des playbooks et des rôles.
        """
        return self.playbooks + self.roles
