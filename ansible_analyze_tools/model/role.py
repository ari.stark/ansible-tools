# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import glob
import os

import yaml

META_FILE = 'meta/main.yml'
TASKS_FILES = 'tasks/*.yml'


class Role:
    """
    Modélisation simplifiée d'un rôle Ansible.

    Ce modèle ne prend en compte que les dépendances vis-à-vis des autres rôles.
    """

    role_dir = None
    name = None
    path = None
    eager = True
    roles = []

    def __init__(self, role_dir, role_name, eager=True, _load_children=True) -> None:
        """
        Construit un modèle de rôle.

        Le constructeur lit le fichier meta et le fichier de tâches du rôle afin de trouver les dépendances.

        :param role_dir: Répertoire menant au rôle.
        :param role_name: Nom du rôle.
        :param eager: Définit si la construction des dépendances sera récursive.
        :param _load_children: Définit s'il faut charger les dépendances.
        """
        super().__init__()
        self.role_dir = role_dir
        self.name = role_name
        self.path = os.path.join(role_dir, role_name)
        self.eager = eager

        if not os.path.isdir(self.path):
            raise ValueError(self.path + ' n\'est pas un chemin valide.')

        self.roles = []
        if _load_children:
            self._find_roles_in_meta()
            self._find_roles_inclusion()

    def to_json(self) -> dict:
        """
        Convertie l'objet au format JSON.

        :return: Une structure JSON.
        """
        json = {'name': self.name}
        if self.roles:
            roles = []
            for role in self.roles:
                roles.append(role.to_json())
            json['roles'] = roles
        return json

    def _find_roles_in_meta(self) -> None:
        """
        Trouve les rôles dans le fichier meta.
        """
        meta_file = os.path.join(self.path, META_FILE)

        if not os.path.isfile(meta_file):
            return

        with open(meta_file, 'r') as file_desc:
            yaml_content = yaml.load(file_desc, Loader=yaml.FullLoader)

        for dependency in yaml_content['dependencies']:
            # Une dépendance a deux formes possibles : le nom du rôle directement ou le nom dans une balise name.
            role_name = dependency['role'] if isinstance(dependency, dict) else dependency
            self.roles.append(Role(self.role_dir, role_name, _load_children=self.eager))

    def _find_roles_inclusion(self) -> None:
        """
        Trouve les rôles dans le fichier tasks/main.yml.
        """
        tasks_files = glob.glob(os.path.join(self.path, TASKS_FILES))

        # Analyse de chaque fichier .yml dans le répertoire tasks.
        for tasks_file in tasks_files:
            with open(tasks_file, 'r') as file_desc:
                yaml_content = yaml.load(file_desc, Loader=yaml.FullLoader)

            if yaml_content is None:
                continue

            for task in yaml_content:
                # Les rôles sont inclus dans les tâches soit avec include_role ...
                if 'include_role' in task:
                    role_name = task['include_role']['name']
                    self.roles.append(Role(self.role_dir, role_name, _load_children=self.eager))
                # ... ou import_role.
                if 'import_role' in task:
                    role_name = task['import_role']['name']
                    self.roles.append(Role(self.role_dir, role_name, _load_children=self.eager))

    def dependencies(self) -> list:
        """
        Regroupe les dépendances.

        :return: Liste des rôles.
        """
        return self.roles

    def has_tasks(self) -> bool:
        """
        Indique si le rôle possède des tâches.

        :return: Vrai si le rôle a des tâches.
        """
        return glob.glob(os.path.join(self.path, TASKS_FILES))
