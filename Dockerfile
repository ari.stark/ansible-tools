FROM python:3.7

COPY . /tmp
RUN cd /tmp && \
    pip install -r requirements.txt

CMD python --version && \
    pip list && \
    cd /tmp/tests && \
    coverage run $(which nosetests) -v && \
    coverage report -m
