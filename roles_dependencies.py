#!/usr/bin/python3
# -*- coding: utf-8 -*-

# (c) 2019, Ari Stark <ari.stark@netcourrier.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import argparse
import os
import re

from graphviz import Digraph

from ansible_analyze_tools.model.playbook import Playbook
from ansible_analyze_tools.model.role import Role

graph = Digraph()

# Stockage pour éviter les doublons
nodes_id = []
links = []


def add_node(element) -> None:
    """
    Ajoute un nœud au graphe.

    Cette fonction vérifie que le nœud n'a pas déjà été ajouté, pour éviter les doublons dans le graphe.

    :param element: Élement à insérer : un playbook ou un rôle.
    """
    if element.name not in nodes_id:
        has_tasks = isinstance(element, Playbook) or element.has_tasks()
        graph.node(element.name,
                   style='solid' if has_tasks else 'dotted')  # S'il n'y a pas de tâche, le nœud est en pointillé.
        nodes_id.append(element.name)


def find_all_roles(path) -> list:
    """
    Trouve tous les rôles contenus dans une arborescence.

    La méthode de détection d'un rôle se fait par la détection d'un répertoire meta
    ou d'un répertoire tasks dans une sous-arborescence.
    Les rôles qui n'ont ni meta ni tâche ne nous intéressent pas.

    :param path: Répertoire de départ contenant les rôles.
    :return: Une liste de rôles.
    """
    roles = []
    lookup_directory = ['meta', 'tasks']  # Répertoires rechechés.

    # Pour chaque répertoire de l'arborescence...
    for root, dirs, files in os.walk(path):
        if set(dirs) & set(lookup_directory):  # Si les répertoires meta ou tasks sont trouvés.
            role_name = re.sub(path + '/?(.*)', r'\1', root)
            roles.append(Role(path, role_name, eager=False))

    roles.sort(key=lambda r: r.name)  # Pour voir si ça change quelque chose dans le CI GitLab
    return roles


def create_graph(roles_path, output_file_name, want_image) -> None:
    """
    Génère un graphe de dépendances de rôles d'un projet complet.

    Cette fonction crée le graphe, parcours l'ensemble des rôles, puis affiche le graphe.

    :param roles_path: Répertoire contenant les rôles.
    :param output_file_name: Nom du fichier de sortie.
    :param want_image: Booléen indiquant s'il faut une image ou un fichier source en sortie.
    """
    # Crée un graphe sans orientation sur la base d'un "spring" modèle.
    global graph
    graph = Digraph('roles', engine='fdp')
    graph.attr('edge', dir='back', arrowtail='ediamond')

    # Pour chaque rôle.
    for role in find_all_roles(roles_path):
        add_node(role)
        for dependency in role.dependencies():
            add_node(dependency)
            if [role.name, dependency.name] not in links:
                graph.edge(role.name, dependency.name)
                links.append([role.name, dependency.name])

    # Écrit le graphe.
    if want_image:
        graph.render(output_file_name, format='png', view=True, cleanup=True)
    else:
        graph.save(output_file_name)


# main
if __name__ == "__main__":
    default_roles_path = '.'
    default_output_file = '/tmp/graph-roles-dependencies.gv'

    parser = argparse.ArgumentParser(description='Build a directed graph of dependencies of roles.')
    parser.add_argument('-r', '--roles-path',
                        help='Path to roles. (Default: {})'.format(default_roles_path),
                        default=default_roles_path,
                        )
    parser.add_argument('-o', '--output-file',
                        help='Output file as e PNG file. (Default: {})'.format(default_output_file),
                        default=default_output_file,
                        )
    output_group = parser.add_mutually_exclusive_group()
    output_group.add_argument('-i', '--image-file',
                              help='Output file as a PNG file. (Default: True)',
                              default=True,
                              action='store_true',
                              dest='image_file',
                              )
    output_group.add_argument('-t', '--text-file',
                              help='Output file as a text file.',
                              action='store_false',
                              dest='image_file',
                              )
    args = parser.parse_args()

    if not os.path.isdir(os.path.expanduser(args.roles_path)):
        raise ValueError("Roles path '{}' doesn't exist or is not a directory.".format(args.roles_path))

    create_graph(args.roles_path, args.output_file, args.image_file)
