[![pipeline status](https://gitlab.com/ari.stark/ansible-tools/badges/master/pipeline.svg)](https://gitlab.com/ari.stark/ansible-tools/commits/master)
[![coverage report](https://gitlab.com/ari.stark/ansible-tools/badges/master/coverage.svg)](https://gitlab.com/ari.stark/ansible-tools/commits/master)

# Projet d'aide à l'analyse de projets Ansible

Ce projet offre plusieurs scripts pour assister l'analyse de projets Ansible.

Il permet, par exemple, d'obtenir une représentation graphique des dépendances d'un playbook.
Plus de précisions plus bas.

## Prérequis

Ce projet utilise Python 3. La liste des modules Python nécessaires se trouve dans requirements.txt.

## Scripts

### playbook_dependencies.py

Ce script permet d'afficher les dépendances d'un playbook donné.

La résolution des dépendances se fait en profondeur ; la résolution est récursive.

L'affichage se fait via une image au format PNG ou via un fichier texte au format DOT.

### roles_dependencies.py

Ce script permet d'afficher l'interdépendance des rôles d'un projet.

Les rôles ne faisant rien et n'étant utilisés par aucun autre rôle ne sont pas affichés.

L'affichage se fait via une image au format PNG ou via un fichier texte au format DOT.

# Licence

GPLv3

# Auteur

Ari Stark
